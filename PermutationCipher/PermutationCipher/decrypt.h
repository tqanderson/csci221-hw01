#include <iostream>
#include <string>
#include <fstream>
#ifndef DECRYPT_H
#define DECRYPT_H

using namespace std;

class Decrypt {
private:
	string fileName;
	int blockSize;
	char permutation;
	int permarr[8];

public:
	Decrypt() {};

	Decrypt(string fn, int bsize, int p) {
		fileName = fn;
		blockSize = bsize;
		permutation = p;
	}

	void setFileName(string fn) {
		fileName = fn;
	}

	void setBlockSize(int bsize) {
		blockSize = bsize;
	}
	//turning a string [e.g. 3142] into a int array to tell us how to permutate
	int setPermutation(string p) {
		int n = p.length();
		char tempVar[8];
		strcpy_s(tempVar, p.c_str());
		for (int i = 0; i < n; i++) {
			cout << tempVar[i];
			permarr[i] = tempVar[i] - '0';
		}
		return permarr[8];
	}

	string const getFileName() {
		return fileName;
	}

	int const getBlockSize() {
		return blockSize;
	}

	int const getPermutation() {
		return permutation;
	}
	//turning a string into a char array, then rearrange characters according to the permutation
	string unPermutate(string unperm) {
		string temp;
		//turn perm string into character array
		int n = unperm.length();
		for (int i = 0; i < n; i++) {
			temp = temp + unperm.substr(permarr[i] - 1, 1);
		}
		//break string into array
		//read in line from file in by block size
		//for each block size, reallocate the array
		return temp;
	}
};
#endif
