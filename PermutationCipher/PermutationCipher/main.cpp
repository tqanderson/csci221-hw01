#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <ctype.h>
#include <algorithm>
#include <cctype>
#include "encrypt.h"
#include "decrypt.h"
using namespace std;

string userPTFile;
int userBlockSize;
string userPermutation;
string userOperation;
Encrypt newEncryption;
Decrypt newDecryption;


void getUserInputs() {
	cout << "Please enter your plaintext filename that you would like to be encrypted: \n";
	cin >> userPTFile;
	cout << "Please enter the block size (2 - 8): \n";
	cin >> userBlockSize;
	cout << "Please enter the permutation(e.g., 2413): \n";
	cin >> userPermutation;
	cout << "Encrypt or decrypt? \n";
	cin >> userOperation;
	transform(userOperation.begin(), userOperation.end(), userOperation.begin(),
		[](unsigned char c) { return std::tolower(c); });


	newEncryption.setFileName(userPTFile);
	newEncryption.setBlockSize(userBlockSize);
	newEncryption.setPermutation(userPermutation);


}

int main() {
	//string myText;
	string outStr;

	cout << "Welcome to the permutation cypher. \n";

	getUserInputs();
	cout << newEncryption.getFileName();

	fstream myReadFile;
	ofstream outdata;

	myReadFile.open(newEncryption.getFileName(), ios::in);
	if (myReadFile.is_open()) {
		string myText;
		while (getline(myReadFile, myText)) {
			string outStr;
			if (userOperation == "encrypt") {
				char tempChar[256];
				strcpy_s(tempChar, myText.c_str());
				int i = 0;
				while (i < myText.length()) {
					int span;
					if (i + newEncryption.getBlockSize() < myText.length()) {
						span = newEncryption.getBlockSize();
					}
					else {
						span = myText.length() - i;
					}
					string encString;
					encString = myText.substr(i, span);
					outStr = outStr + newEncryption.permutate(encString);
					i = i + span;
				}
			}
			else if (userOperation == "decrypt") {
				char tempChar[256];
				strcpy_s(tempChar, myText.c_str());
				int i = 0;
				while (i < myText.length()) {
					int span;
					if (i + newDecryption.getBlockSize() < myText.length()) {
						span = newDecryption.getBlockSize();
					}
					else {
						span = myText.length() - i;
					}
					string decString;
					decString = myText.substr(i, span);
					outStr = outStr + newDecryption.unPermutate(decString);
					i = i + span;
				}
			}
			cout << outStr << '\n';
		}
	}
}


	//take input file
	//get block size or less characters
	//store into an array
	//get permutation
	//pass through decryption
	//as decrypted, assemble a string
	//write string to an output file
